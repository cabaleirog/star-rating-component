const fs = require('fs-extra');
const concat = require('concat');

(async function build() {
    const files = [
        './dist/star-rating/runtime.js',
        './dist/star-rating/polyfills.js',
        './dist/star-rating/scripts.js',
        './dist/star-rating/main.js',
    ]

    await fs.ensureDir('WebComponent')

    await concat(files, 'WebComponent/star-rating.js');

    await fs.copyFile('./dist/star-rating/styles.css', 'WebComponent/styles.css')

    // await fs.copy('./dist/star-rating/assets/', 'WebComponent/assets/' )

})()
