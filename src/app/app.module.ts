import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';

import { AppComponent } from './app.component';
import { RatingComponent } from './rating/rating.component';
import { createCustomElement } from '@angular/elements';

@NgModule({
    declarations: [
        AppComponent,
        RatingComponent
    ],
    imports: [
        BrowserModule
    ],
    providers: [],
    //   bootstrap: [AppComponent],
    entryComponents: [RatingComponent]
})
export class AppModule {

    constructor(private injector: Injector) { }

    ngDoBootstrap() {
        const StarRatingElement = createCustomElement(RatingComponent, { injector: this.injector });
        customElements.define('star-rating', StarRatingElement);
    }
}
