import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-rating',
    templateUrl: './rating.component.html',
    styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {

    @Input() rating: number;
    @Input() maxRating = 5;
    @Input() starSize = 25;  // in pixels
    @Input() strokeWidth = 15;  // in pixels
    @Input() fillColor = 'rgba(255, 204, 102, 1)';
    @Input() strokeColor = 'rgba(255, 204, 102, 1)';
    @Input() unratedColor = 'rgba(255, 204, 102, 0.4)';
    @Input() disableUnrated = true;

    private id: string;

    starRating: any[] = [];

    constructor() {
        this.id = (Math.floor(Math.random() * Number.MAX_SAFE_INTEGER)).toString();
    }

    ngOnInit() {
        if (this.rating === undefined) {
            this.rating = 0;
            if (this.disableUnrated) { this.strokeColor = this.unratedColor; }
        }

        // TODO: Review why variables are of type strings
        if (+this.rating > +this.maxRating) {
            if (+this.rating - +this.maxRating > 0.001) {
                console.warn(`Rating (${this.rating}) is larger than maxRating (${this.maxRating}).`);
            }
            this.rating = this.maxRating;
        }

        for (let i = 0; i < this.maxRating; i++) {
            this.starRating.push({
                percentage: Math.max(0, Math.min(1, this.rating - i)),
                id: `grad_${this.id}_${i}`
            });
        }
    }

}
